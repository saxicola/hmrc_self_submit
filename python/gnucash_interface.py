#!/bin/env python3
# -*- coding: utf-8 -*-
#(C) Mike Evans 2019 <mikee:saxicola.co.uk>
'''
gnucash python interface
Not much here right now and may (probably) need to add methods to GnuCash's python bindings
to work.
Only works with XML file storage at present.
TODO: All the database backends. It may be simpler just to query the database directly
but than would mean more code which could diversify and break.  Then all the woes
of the world will come crashing down opon us and we will be smited mightely.
'''

from __future__ import division
import gnucash
import os
import sys
#import date
import datetime
import time
from time import mktime
import numpy as np

import logging
logging.basicConfig(level=logging.DEBUG, format='%(module)s: LINE %(lineno)d: %(levelname)s: %(message)s')
DEBUG = logging.debug
INFO = logging.info

session = None
book= None
root = None
data_store = None

print (sys.version_info )

def open_book(account_file):
    '''
    Open a GnuCash file, or database, and load the data.
    TODO: Add sql database connections too.
    @param String
    '''
    try: session = gnucash.Session("xml://%s" % account_file, True, False, False)
    except:
        print (' Failed to open GnuCash file.  Please check and try again/')
        quit(0)
    root = session.book.get_root_account()
    book = session.book
    return session, book, root


def close_book(session):
    session.end()
    session.destroy()


def get_expenses(account, period_from, period_to):
    '''
    @param String: Account name, maybe
    @param String: Date
    @param String: Date
    '''
    dt1 = int(time.mktime(datetime.datetime.strptime(period_from,'%Y-%m-%d').timetuple()))
    dt2 = datetime.datetime.strptime(period_to,'%Y-%m-%d')
    expenses = None
    expenses_total = 0
    print("EXPENSES\n")
    accounts = root.get_children_sorted()
    for account in accounts:
        if account.name == 'Expenses':
            expenses_total = account.GetBalanceChangeForPeriod(dt1,dt2,False).num() / account.GetBalanceChangeForPeriod(dt1,dt2,True).denom()
            DEBUG(expenses_total )
            expenses = account.get_children_sorted()
            break
    for expense in expenses:
        dte = expense.GetBalanceChangeForPeriod(dt1,dt2,True).num() / expense.GetBalanceChangeForPeriod(dt1,dt2,True).denom()
        #DEBUG("{} {}".format(expense.name,dte))
        if dte == 0: continue
        expenses_total += dte
        print(expense.name,  dte)
    # As a check the values printed below should match
    print ("Expenses Total =", account.GetBalanceChangeForPeriod(dt1,dt2,True).num() /account.GetBalanceChangeForPeriod(dt1,dt2,True).denom())
    print ("Expenses Total =", round(expenses_total,2))
    print('\n')
    return round(expenses_total,2)


def get_income(account, period_from, period_to):
    '''
    @param String: Account name, maybe
    @param String
    @param String
    '''
    dt1 = datetime.datetime.strptime(period_from,'%Y-%m-%d')
    dt2 = datetime.datetime.strptime(period_to,'%Y-%m-%d')
    incomes = None
    income_total = 0
    print("\n\nINCOME\n")

    accounts = root.get_children_sorted()
    for account in accounts:
        if account.name == 'Income':
            income_total = account.GetBalanceChangeForPeriod(dt1,dt2,False).num() / account.GetBalanceChangeForPeriod(dt1,dt2,False).denom()
            incomes = account.get_children_sorted()
            break
    for income in incomes:
        dti = income.GetBalanceChangeForPeriod(dt1,dt2,True).num() / income.GetBalanceChangeForPeriod(dt1,dt2,True).denom()
        if dti == 0: continue
        income_total += dti
        print(income.name,  -dti)
    # As a check the values printed below should match
    print ("Income Total =", -account.GetBalanceChangeForPeriod(dt1,dt2,True).num() /account.GetBalanceChangeForPeriod(dt1,dt2,True).denom())
    print ("Income Total =", round(-income_total,2))
    print ('\n')
    return round(income_total,2)


def get_income_non_taxable(account, period_from, period_to):
    '''
    @param String: Account name
    @param String
    @return Double Incpme fpr period.
    '''
    #Do some stuff here
    return #a number


def get_acct_balance_on_date(account, date_string):
    '''
    @param String: account
    @param String: date_string
    @return Double: Account balance at date
    '''
    unixtime = time.mktime(datetime.datetime.strptime(date_string,'%Y-%m-%d').timetuple())
    GncNumeric = account.GetBalanceAsOfDate(unixtime) # A random time
    return -(GncNumeric.num()*1.0/ GncNumeric.denom()*1.0)


# Test code
if __name__ == "__main__":
    try:
        print (sys.argv[1]) # Date range required too probably.
        account_file = sys.argv[1] # Or from a config file
        session, book, root =  open_book(account_file)
        # Accounting periods will be obtained from HMRC
        #get_income_non_taxable('','2018-04-06','2019-04-05') # Sample accounting periods
        get_income('','2018-04-06','2019-04-06') # Sample accounting periods
        get_expenses('','2018-04-06','2019-04-05')
        session.end()
        session.destroy()
    except:
        print ("You need to supply an account file")
        raise
        quit(1)
