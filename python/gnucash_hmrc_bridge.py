#!/bin/env python3
# -*- coding: utf-8 -*-
'''
(C) Mike Evans 2019 <mikee:saxicola.co.uk>

Making Tax Difficult interface to HMRC's API

Works with more recent versions of GnuCash only that support python3.
Since we in the UK we can predict the reporting intervals as:
YYYY-04-06 YYYY-07-06 YYYY-10-06 YYYY-01-06 YYYY-04-06

This is stll test code using the test credentials.

This is not officially affiliated with GnuCash.
'''


import argparse
import datetime
import sys
sys.path.insert(0,'/home/mikee/progs/lib64/python3.10/site-packages')
import gnucash
from gnucash import gnucash_business
import logging
import os
import requests
import version
import webbrowser
from http.server import HTTPServer, BaseHTTPRequestHandler
from urllib.parse import urlparse, parse_qs
from optparse import OptionParser

import gettext
_ = gettext

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio


logging.basicConfig(level=logging.DEBUG, format='%(module)s: LINE %(lineno)d: %(levelname)s: %(message)s')
DEBUG = logging.debug
INFO = logging.info



# TODO: This will be from somewhere else untimately possibly from GnuCash.
import app_creds
import vat_user
# TODO: config will not be hard coded either.
config = {"account_file" : "", "income_account" : "Income", "expenses_account" : "Expenses", "liability_account":"Liabilities","period_from" : "2018-04-06", "period_to" : "2018-07-06"}


parser = argparse.ArgumentParser()
parser.add_argument('--version', action='version', version=version.__version__)
parser.add_argument("-v", "--verbose", help="Output INFO statements", action="store_true")
parser.add_argument("-d", "--debug", help="Output DEBUG and INFO statements",  action="store_true")

args = parser.parse_args()

# Set up debugging output level
logging.basicConfig(level=logging.INFO, format='%(module)s: LINE %(lineno)d: %(levelname)s: %(message)s')
if args.verbose:
    logging.disable(logging.DEBUG)
elif args.debug:
    pass
else:
    logging.disable(logging.DEBUG)
    logging.disable(logging.INFO)


class RequestHandler(BaseHTTPRequestHandler):
    '''
    This is the required redirect URI registered for the application at HMRC developer service
    http://localhost:9090
    See start_server() in pyselftax()
    '''
    has_data = 0
    allow_reuse_address = True
    access_token = None
    session = None

    def do_GET(self):
        self.timeout = 3
        request_path = self.path
        DEBUG("\n----- Request Start ----->\n")
        print(request_path)
        print(self.headers)
        DEBUG("<----- Request End -----\n")
        if (sys.version_info > (3, 0)):
            self.server.access_token = parse_qs(urlparse(self.path).query).get('code', None)
        else :
            self.server.access_token = urlparse.parse_qs(urlparse.urlparse(self.path).query).get('code', None)

        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        self.wfile.write('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'.encode())
        self.wfile.write('<html>'.encode())
        self.wfile.write('  <head>'.encode())
        self.wfile.write('    <title>Server Info</title>'.encode())
        self.wfile.write('  </head>'.encode())
        self.wfile.write('  <body>'.encode())
        self.wfile.write('  <h1>Auth key obtained</h1>'.encode())
        self.wfile.write('  <p>Now go back to the app.</p>'.encode())
        self.wfile.write('  </body>'.encode())
        self.wfile.write('</html>'.encode())

        self.end_headers()
        self.has_data = 1
        self.finish()
        self.server.stop = True

    do_DELETE = do_GET



class GnuCashMTD(Gtk.ApplicationWindow):
    def __init__(self):
        DEBUG("Version = {}".format(version.__version__))
        self.moneys = {} # Store all the GnuCash query results in this
        builder = Gtk.Builder()
        self.gladefile = os.path.join(os.path.dirname(__file__),"ui/main_window.glade")
        builder.add_from_file(self.gladefile)
        builder.connect_signals(self)
        # More UI stuff
        self.treeview  = builder.get_object('treeview_periods')
        window = builder.get_object("window1")
        self.text_area = builder.get_object("text_area")
        self.text_buffer = self.text_area.get_buffer()
        # Use GnuCash's recent file list to populate a dropdown
        self.BASE_KEY = "org.gnucash.history"
        self.settings = Gio.Settings.new(self.BASE_KEY)
        keys = self.settings.keys()
        self.recent_files_list = builder.get_object("liststore_last_files")
        self.recent_files_dropdown = builder.get_object("combo_last_files")
        for key in keys:
            if self.settings.get_string(key):
                self.recent_files_list.append([key, self.settings.get_string(key)])
                DEBUG(key)
                DEBUG(self.settings.get_string(key))
        window.show_all()
        Gtk.main()
        return


    def on_combo_last_files_changed(self, combo):
        tree_iter = combo.get_active_iter()
        model = combo.get_model()
        row_id, URI = model[tree_iter][:2]
        DEBUG("Selected: ID = {}, URI = {}".format(row_id, URI))
        config["account_file"] = URI
        return


    #on_button_close = on_destroy
    def on_destroy(self, *args):
        DEBUG("Bye!")
        Gtk.main_quit()


    def add_filters(self, dialog):
        filter_gnucash = Gtk.FileFilter()
        filter_gnucash.set_name('Gnucash files')
        filter_gnucash.add_pattern("*.gnucash") # TODO
        dialog.add_filter(filter_gnucash)


    def on_button_save(self, *args):
        '''
        UNUSED. REMOVE ME.
        Save the current settings to file
        '''
        dialog = Gtk.FileChooserDialog("Please choose a file", None,
            Gtk.FileChooserAction.SAVE,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
             Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        dialog.set_modal(True)
        dialog.set_default_size(600, 400)
        self.add_filters(dialog)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            DEBUG("File selected: {}".format(dialog.get_filename()))
            path = dialog.get_filename()
            dialog.destroy()
            # Save the file
            return True
        else:
            dialog.destroy()
            return False
        return False


    def get_app_creds(self):
        '''
        Obtain the creds from somewhere(?) that keeps HMRC happy.
        '''
        creds = {}
        return creds


    def get_access_token(self, code):
        '''
        Get access token.
        Using POST
        AUTHORIZATION-CODE is the 6 digit code you get from HMRC
        @param string 6 digit code
        '''
        r = requests.post("https://test-api.service.hmrc.gov.uk/oauth/token", \
            data = {"client_id":app_creds.client_id,"client_secret": app_creds.client_secret, \
            "code":code,"grant_type": "authorization_code", \
            "redirect_uri": "http://localhost:9090"})
        #DEBUG(r.content)
        #DEBUG(r.json())
        if r.status_code != 200:
            DEBUG(r.json())
            if r.json()['error_description'] == 'code is invalid':
                DEBUG("No account file selected")
                dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.WARNING,
                    Gtk.ButtonsType.OK, "Code is invalid")
                dialog.run()
                dialog.destroy()
                DEBUG ("An error occurred")
                return
        else:
            DEBUG(r.json())
            DEBUG(r.url)
            return r.json()['access_token']


    def get_auth_code(self):
        '''
        Request authorisation.
        Using GET
        '''
        r = requests.get( "https://test-api.service.hmrc.gov.uk/oauth/authorize?response_type=code", \
            headers = {"Accept":"application/vnd.hmrc.1.0+json", \
            "Content-Type" : "application/json"} ,\
            params = {"client_id":app_creds.client_id, \
            "scope" : "read:individual-tax read:self-assessment write:self-assessment write:vat read:vat",  \
            "redirect_uri": "http://localhost:9090"})

        if r.status_code != 200:
            print ("An error occurred")
            if r.headers.get('Content-Type') == 'application/json':
                # This is usually an error and contains the error condition
                DEBUG(r.json())
            quit(1)
        else:
            r.headers.get('Content-Type') == 'text/html; charset=utf-8'
            webbrowser.open(r.url)


    def on_submit_earnings(self, widget):
        '''
        Submit a quarterly tax return
        TODO: Nearly all this
        '''
        return
        code_url = self.get_auth_code()
        code = self.start_server()
        DEBUG(code)
        token = self.get_access_token(code)
        json_data = json.dumps(self.moneys)
        DEBUG(json_data)
        return
        self.income_for_period_add(token, test_user['nino'],'XDIS41069900810', json_data)
        return


    def get_user_pw(self, message, title=''):
        # Returns user input as a string or None
        # If user does not input text it returns None, NOT AN EMPTY STRING.

        dialogWindow = Gtk.MessageDialog(None,
                            Gtk.DialogFlags.MODAL,# | Gtk.DialogFlags.DESTROY_WITH_PARENT,
                            Gtk.MessageType.QUESTION,
                            Gtk.ButtonsType.OK_CANCEL,
                            message)
        dialogWindow.set_title(title)
        dialogBox = dialogWindow.get_content_area()
        userEntry = Gtk.Entry()
        userEntry.set_visibility(False)
        userEntry.set_invisible_char("*")
        userEntry.set_size_request(100,0)
        dialogBox.pack_end(userEntry, False, False, 0)
        dialogWindow.show_all()
        response = dialogWindow.run()
        text = userEntry.get_text()
        dialogWindow.destroy()
        if (response == Gtk.ResponseType.OK) and (text != ''):
            return text
        else:
            return None


    def open_book(self, account_file):
        '''
        Open a GnuCash file, or database, and load the data.
        The default is to open a .gnucash file.
        DONE: Add sql database connections too.
        @param String Full account file path
        '''
        DEBUG(account_file)
        if account_file.startswith("/"): # A local file on a Linux system TODO
            try: session = gnucash.Session("xml://{}".format(account_file), True, False, False)
            except:
                print (' Failed to open GnuCash file.  Please check and try again/')
                raise
                quit(0)
        elif account_file.startswith("sqlite3"):
            session = gnucash.Session("{}".format(account_file), is_new=False, ignore_lock=True)
        elif account_file.startswith("mysql"):
            password = self.get_user_pw("Enter your password", "Database  password")
            if not password: return False, False , False
            uri = account_file.split('@')
            account_file = "{}:{}@{}".format(uri[0],password, uri[1]) # Not a real password!
            DEBUG(account_file)
            session = gnucash.Session('{}'.format(account_file), is_new=False, ignore_lock=True)
        root = session.book.get_root_account()
        book = session.book
        return session, book, root


    def close_book(self, session):
        session.end()
        session.destroy()


    def get_income(self, root):
        '''
        @param String: Account name, maybe
        @param String
        @param String
        '''
        dt1 = datetime.datetime.strptime(config['period_from'],'%Y-%m-%d')
        dt2 = datetime.datetime.strptime(config['period_to'],'%Y-%m-%d')
        incomes = None
        income_total = 0
        print("\n\nINCOME\n")
        taxable_income = 0
        tax_paid_income = 0
        accounts = root.get_children_sorted()
        for account in accounts:
            if account.GetType() == gnucash.gnucash_core_c.ACCT_TYPE_INCOME:
                income_total = account.GetBalanceChangeForPeriod(dt1,dt2,False).num() / account.GetBalanceChangeForPeriod(dt1,dt2,False).denom()
                incomes = account.get_children_sorted()
                break
        for income in incomes:
            dti = income.GetBalanceChangeForPeriod(dt1,dt2,True).num() / income.GetBalanceChangeForPeriod(dt1,dt2,True).denom()
            if dti == 0: continue
            DEBUG(income.name)
            # TODO: Don't hard code these.
            if income.GetTaxRelated() : taxable_income += dti
            elif not income.GetTaxRelated() : tax_paid_income += dti
            income_total += dti
            print(income.name,  -dti)
        # As a check the values printed below should match
        print ("Income Total =", -account.GetBalanceChangeForPeriod(dt1,dt2,True).num() /account.GetBalanceChangeForPeriod(dt1,dt2,True).denom())
        print ("Income Total =", round(-income_total,2))
        print ('\n')
        return -tax_paid_income, -taxable_income


    def get_expenses(self, root):
        '''

        '''
        dt1 = datetime.datetime.strptime(config['period_from'],'%Y-%m-%d')
        dt2 = datetime.datetime.strptime(config['period_to'],'%Y-%m-%d')
        expenses = None
        expenses_total = 0
        print("EXPENSES\n")
        accounts = root.get_children_sorted()
        for account in accounts:
            if account.GetType() == gnucash.gnucash_core_c.ACCT_TYPE_EXPENSE:
                expenses_total = account.GetBalanceChangeForPeriod(dt1,dt2,False).num() / account.GetBalanceChangeForPeriod(dt1,dt2,True).denom()
                DEBUG(expenses_total )
                expenses = account.get_children_sorted()
                break
        for expense in expenses:
            dte = expense.GetBalanceChangeForPeriod(dt1,dt2,True).num() / expense.GetBalanceChangeForPeriod(dt1,dt2,True).denom()
            #DEBUG("{} {}".format(expense.name,dte))
            if dte == 0: continue
            expenses_total += dte
            print(expense.name,  dte)
        # As a check the values printed below should match
        print ("Expenses Total =", account.GetBalanceChangeForPeriod(dt1,dt2,True).num() /account.GetBalanceChangeForPeriod(dt1,dt2,True).denom())
        print ("Expenses Total =", round(expenses_total,2))
        print('\n')
        return round(expenses_total,2)


    def get_liabilites(self, root):
        '''
        These will include VAT payable to HMRC.
        There are 3 VAT rates, std, reduced and zero. All have to accounted for.
        '''
        dt1 = datetime.datetime.strptime(config['period_from'],'%Y-%m-%d')
        dt2 = datetime.datetime.strptime(config['period_to'],'%Y-%m-%d')
        vat_accounts = None
        vat_total = 0
        vat_sales = 0
        vat_purchases = 0
        vat_EEC = 0
        accounts = root.get_children_sorted()
        for account  in accounts:
            if account.name == "VAT": #config['liability_account']:
                vat_accounts = account.get_children_sorted()
                break
        for v in vat_accounts:
            if v.name == "Purchases":
                vat_purchases = v.GetBalanceChangeForPeriod(dt1,dt2,True).num() / v.GetBalanceChangeForPeriod(dt1,dt2,True).denom()
                DEBUG("{} = {}".format(v.name,vat_purchases))
            elif v.name == "Sales":
                vat_sales = v.GetBalanceChangeForPeriod(dt1,dt2,False).num() / v.GetBalanceChangeForPeriod(dt1,dt2,False).denom()
                DEBUG("{} = {}".format(v.name,vat_sales))
            elif v.name == "EEC":
                vat_EEC = v.GetBalanceChangeForPeriod(dt1,dt2,False).num() / v.GetBalanceChangeForPeriod(dt1,dt2,False).denom()
                DEBUG("{} = {}".format(v.name,vat_EEC))
        return round(vat_sales,2), round(vat_purchases,2), round(vat_EEC,2)


    def on_button_query_clicked(self, widget):
        '''
        Get earnings and/or VAT numbers for period.
        '''
        DEBUG("Querying GnuCash")
        DEBUG(config['account_file'])
        if config['account_file'] == "" :
            DEBUG("No account file selected")
            dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.WARNING,
                Gtk.ButtonsType.OK, "No account file selected")
            dialog.run()
            dialog.destroy()
            return
        session, book, root = self.open_book(config['account_file'])
        if not session:
            DEBUG("Something went wrong.")
            return
        income_nont, income_t = self.get_income(root)
        self.moneys['income_nont'] = income_nont
        self.moneys['income_t'] = income_t
        expenses = self.get_expenses(root)
        self.moneys['expenses'] = expenses
        vat_sales, vat_purchases, vat_EEC = self.get_liabilites(root)
        self.moneys['vat_sales'] = vat_sales
        self.moneys['vat_purchases'] = vat_purchases
        self.moneys['vat_EEC'] = vat_EEC
        self.text_buffer.set_text("From: {}\nTo: {}\nTaxable Income = £{}\nTax Paid income = £{}\nExpenses = £{}\nVAT: Sales = {}\nVAT: Purchases = {}\nVAT: EEC = {}".format(config['period_from'], config['period_to'], income_t,income_nont, expenses, vat_sales, vat_purchases, vat_EEC))
        self.close_book(session)


    def income_for_period_add(self, accessToken, nino, selfEmploymentId, json_data):
        '''
        Create a self-employment periodic update for submission of periodic data i.e incomes and expenses. Submissions which do not include any incomes or expenses are invalid. You should submit zero values if you do not have incomes or expenses.

        Valid payload may contain either expenses or consolidatedExpenses elements.
        @param accessToken
        @param nino string
        @param selfEmploymentId An identifier for the self-employment business, unique to the customer.
        @param json frame of account data
        Example json data for a short/consolidated tax return:
        json_data = {
        "from": "2017-04-06",
        "to": "2017-07-04",
        "incomes": {
        "turnover": {
          "amount": 100.25
        },
        "other": {
          "amount": 100.25
        }
        },
        "consolidatedExpenses": 100.25
        }
        '''
        r = requests.post( "https://test-api.service.hmrc.gov.uk//self-assessment/ni/{}/self-employments/{}/periods".format(nino, selfEmploymentId), \
         headers = {"Accept":"application/vnd.hmrc.2.0+json" ,\
            "Authorization": "Bearer " + accessToken,\
            "Content-Type":"application/json"},\
            json =json_data)
        DEBUG(r.status_code)
        return r.status_code


    def start_server(self):
        '''
        This just processes one request and returns the access token
        '''
        port = 9090
        try: server.socket.close()# Just in case one is open
        except: pass
        server = HTTPServer(('', port), RequestHandler)
        server.handle_request()
        server.socket.close()
        return server.access_token

    def  on_submit_vat(self, widget):
        return


    def get_business_obligations(self, access_token, nino, selfEmploymentId):
        '''
        # Obligations are when HMRC decide you need to submit and for what period
        self.get_business_obligations(token, test_user['nino'], 'XDIS41069900810')['obligations']
        '''
        r = requests.get( "https://test-api.service.hmrc.gov.uk//self-assessment/ni/{}/self-employments/{}/obligations"\
            .format(nino, selfEmploymentId),
            headers = {"Accept":"application/vnd.hmrc.2.0+json", \
            "Authorization": "Bearer " + access_token})
        if r.ok:
            DEBUG(r.json)
            self.obligations = r.json()
        else:
            DEBUG(r.status_code)
            DEBUG(r.content)
            return None


    ################# END GnuCashMTD #####################

# MAIN
if __name__ == "__main__":
    app = GnuCashMTD()
