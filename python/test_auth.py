#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#(C) Mike Evans 2019 <mikee:saxicola.co.uk>
# Works in Linux (tested on Fedora36) only as is.
# Services need to be subscribed to after registering your application.
# Test API user data, derived from running app.create_test_user() and printing the resulting json
# This is a DUMMY user and is not strictly secret.
# The userID and password will be asked for in the pop-up browser page, served by HMRC, to
# authorise the stuff.


test_user = {'userId': '243156678002', 'password': 'agypeh0z5qbs', 'userFullName': 'Paige Williams', 'emailAddress': 'paige.williams@example.com', 'individualDetails': {'firstName': 'Paige', 'lastName': 'Williams', 'dateOfBirth': '1973-03-16', 'address': {'line1': '13 Mortimer Square', 'line2': 'Ventnor', 'postcode': 'TS20 1PA'}}, 'saUtr': '1171940743', 'nino': 'SK773256A', 'mtdItId': 'XLIT00838861643', 'eoriNumber': 'GB499382950770', 'groupIdentifier': '302438507416'}


import logging
logging.basicConfig(level=logging.DEBUG, format='%(module)s: LINE %(lineno)d: %(levelname)s: %(message)s')
DEBUG = logging.debug
INFO = logging.info

import sys

# This may not be required depending on your GnuCash install.  Mine is slightly customised.
# and is not a default build system install. I have issues with the current code...
sys.path.insert(0,'/home/mikee/progs/lib64/python3.10/site-packages') # EDIT OR COMMENT
import gnucash_interface
import requests
import webbrowser
from datetime import datetime, date
if (sys.version_info > (3, 0)):
    from http.server import HTTPServer, BaseHTTPRequestHandler
    from urllib.parse import urlparse, parse_qs
else:
    from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
    import urlparse
from optparse import OptionParser

import app_creds
import vat_user


class RequestHandler(BaseHTTPRequestHandler):
    '''
    This is the required redirect URI registered for the application at HMRC developer service
    http://localhost:9090
    See start_server() in Pyselftax()
    '''
    has_data = 0
    allow_reuse_address = True
    access_token = None


    def do_GET(self):
        self.timeout = 3
        request_path = self.path
        DEBUG("\n----- Request Start ----->\n")
        print(request_path)
        print(self.headers)
        DEBUG("<----- Request End -----\n")
        if (sys.version_info > (3, 0)):
            self.server.access_token = parse_qs(urlparse(self.path).query).get('code', None)
        else :
            self.server.access_token = urlparse.parse_qs(urlparse.urlparse(self.path).query).get('code', None)

        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        self.wfile.write('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'.encode())
        self.wfile.write('<html>'.encode())
        self.wfile.write('  <head>'.encode())
        self.wfile.write('    <title>Server Info</title>'.encode())
        self.wfile.write('  </head>'.encode())
        self.wfile.write('  <body>'.encode())
        self.wfile.write('  <h1>Auth key obtained</h1>'.encode())
        self.wfile.write('  <p>Now go back to the app.</p>'.encode())
        self.wfile.write('  </body>'.encode())
        self.wfile.write('</html>'.encode())

        self.end_headers()
        self.has_data = 1
        self.finish()
        self.server.stop = True

    do_DELETE = do_GET


################################################################################
class  Pyselftax(object):
    def __init__(self):
        return

    def get_auth_code(self):
        '''
        Request authorisation.
        Using GET
        '''
        r = requests.get( "https://test-api.service.hmrc.gov.uk/oauth/authorize?response_type=code", \
            headers = {"Accept":"application/vnd.hmrc.1.0+json", \
            "Content-Type" : "application/json"} ,\
            params = {"client_id":app_creds.client_id, \
            "scope" : "read:individual-tax read:self-assessment write:self-assessment write:vat read:vat",  \
            "redirect_uri": "http://localhost:9090"})

        if r.status_code != 200:
            print ("An error occurred")
            if r.headers.get('Content-Type') == 'application/json':
                # This is usually an error and contains the error condition
                DEBUG(r.json())
            quit(1)
        else:
            r.headers.get('Content-Type') == 'text/html; charset=utf-8'
            INFO(r.url)
            webbrowser.open(r.url)




    def get_access_token(self, code = None):
        '''
        Get access token.
        Using POST
        AUTHORIZATION-CODE is the 6 digit code you get from HMRC
        @param string 6 digit code
        '''
        if code:
            r = requests.post("https://test-api.service.hmrc.gov.uk/oauth/token", \
            data = {"client_id":app_creds.client_id,"client_secret": app_creds.client_secret, \
            "code":code, "grant_type": "authorization_code", \
            "redirect_uri": "http://localhost:9090"})
        else:
            r = requests.post("https://test-api.service.hmrc.gov.uk/oauth/token", \
            data = {"client_id":app_creds.client_id,"client_secret": app_creds.client_secret, \
            "grant_type": "client_credentials", \
            "redirect_uri": "http://localhost:9090"})
        #DEBUG(r.content)
        #DEBUG(r.json())
        if r.status_code != 200:
            DEBUG(r.json())
            if r.json()['error_description'] == 'code is invalid':
                print ("An error occurred")
                quit(1)
        else:
            DEBUG(r.json())
            DEBUG(r.url)
            return r.json()['access_token']


    def create_test_user(self, access_token):
        '''
        Test users are now more persistent apparently rather than being cleared down regularly.
        @return json object containing the data.
        '''
        r = requests.post("https://test-api.service.hmrc.gov.uk/create-test-user/individuals", \
            headers = {"Accept":"application/vnd.hmrc.1.0+json", "Content-Type" : "application/json", \
            "Authorization" :"Bearer " + access_token}, \
            json = {"serviceNames": [   "national-insurance","self-assessment","mtd-income-tax",\
             "customs-services"  ]})
        DEBUG(r.json())
        if r.ok: return r.json()
        else: return None


    def create_test_vat_individual(self):
        r = requests.post("https://test-api.service.hmrc.gov.uk/create-test-user/individuals", \
            headers = { "Accept":"application/vnd.hmrc.1.0+json", "Content-Type" : "application/json", \
            "Authorization" :"Bearer " + app_creds.server_token},
            json = {  "serviceNames": [    "national-insurance",    "self-assessment",    "mtd-income-tax",    "customs-services",    "mtd-vat"  ]})
        DEBUG(r.json())
        if r.ok: return r.json()
        else: return None



    def list_all_businesses(self,accessToken, nino):
        '''
        @param accessToken
        @param nino
        '''
        r = requests.get( "https://test-api.service.hmrc.gov.uk//self-assessment/ni/{}/self-employments".format(nino),\
        headers = {"Accept":"application/vnd.hmrc.2.0+json" ,\
        "Authorization": "Bearer " + accessToken\
        })
        DEBUG(r.json())
        if r.ok: return r.json()
        else: return None

    def income_for_period_add(self, accessToken, nino, selfEmploymentId, json_data):
        '''
        Create a self-employment periodic update for submission of periodic data i.e incomes and expenses. Submissions which do not include any incomes or expenses are invalid. You should submit zero values if you do not have incomes or expenses.

        Valid payload may contain either expenses or consolidatedExpenses elements.
        @param accessToken
        @param nino string
        @param selfEmploymentId An identifier for the self-employment business, unique to the customer.
        @param json frame of account data
        '''
        r = requests.post( "https://test-api.service.hmrc.gov.uk//self-assessment/ni/{}/self-employments/{}/periods".format(nino, selfEmploymentId), \
         headers = {"Accept":"application/vnd.hmrc.2.0+json" ,\
            "Authorization": "Bearer " + accessToken,\
            "Content-Type":"application/json"},\
            json =json_data)
        DEBUG(r.status_code)
        return r.status_code

    def get_income_for_period(self, accessToken, nino, selfEmploymentId, period_id):
        '''
        @param accessToken
        @param nino string
        @param selfEmploymentId An identifier for the self-employment business, unique to the customer.
        @param period_id obtained from the json:id returned from list_all_businesses
        Get a single self-employment periodic update for a given identifier.
        @param nino string
        @param selfEmploymentId An identifier for the self-employment business, unique to the customer.
        @param period_id An identifier for the update period, unique to the customer's self-employment business.
        '''
        r = requests.get( "https://test-api.service.hmrc.gov.uk//self-assessment/ni/{}/self-employments/{}/periods/{}".format(nino, selfEmploymentId, period_id),\
        headers = {"Accept":"application/vnd.hmrc.2.0+json" ,\
            "Authorization": "Bearer " + accessToken,\
            })
        if r.ok: return r.json()
        else: return None


    def add_business(self, accessToken, nino):
        '''
        @param accessToken
        @param Stringnino
        @param String National Insurance Number
        '''
        r = requests.post( f"https://test-api.service.hmrc.gov.uk/self-assessment/ni/{nino}/self-employments",\
            headers = {"Accept":"application/vnd.hmrc.2.0+json" ,\
            "Authorization": "Bearer " + accessToken,\
            "Content-Type":"application/json"},
        json = {
        "accountingPeriod": {
        "start": "2021-04-06",
        "end": "2022-04-05"
        },
        "accountingType": "CASH",
        "commencementDate": "2022-01-01",
        "tradingName": "Acme Ltd.",
        "businessAddressLineOne": "1 Acme Rd.",
        "businessAddressLineTwo": "London",
        "businessAddressLineThree": "Greater London",
        "businessAddressLineFour": "United Kingdom",
        "businessPostcode": "A9 9AA"
        })
        DEBUG(r.status_code)
        DEBUG(r)
        #DEBUG(r.json())


    def get_individual_income(self,  accessToken, nino, selfEmploymentId, taxYear):
        '''
        @param accessToken
        @param nino string National Insurance Number
        @param selfEmploymentId An identifier for the self-employment business, unique to the customer.
        @param String Tax year like '2017-18'
        @return json object containing the data.
        '''
        r = requests.get("https://test-api.service.hmrc.gov.uk/self-assessment/ni/{}/self-employments/{}/{}"\
        .format(nino, selfEmploymentId,taxYear), \
        headers = {"Accept":"application/vnd.hmrc.2.0+json", \
        "Authorization": "Bearer " + accessToken,\
        "tax_year":"2017-18"})
        if r.status_code == 404:
            print (r.status_code)
            return None
        if r.ok: return r.json()
        else: return None


    def create_test_income(self, accessToken, utr, period):
        '''
        @param String accessToken
        @param String UTR
        @param String period
        @return json object containing the data.
        '''
        r = requests.post("https://test-api.service.hmrc.gov.uk//individual-paye-test-support/sa/{}/income/annual-summary/{}".format(accessToken, utr, period), \
            headers = {"Accept":"application/vnd.hmrc.2.0+json",\
            "Authorization": "Bearer " + accessToken } )
        DEBUG(r.status_code)
        if r.ok: return r.json()
        else: return None


    def access_data(self, accessToken):
        '''
        Testing the hello
        @param string: access token
        '''
        r = requests.get( "https://test-api.service.hmrc.gov.uk/hello/user", \
            headers = {"Accept":"application/vnd.hmrc.1.1+json", "Content-Type" : "application/json", \
            "Authorization": "Bearer " + accessToken} )
        if r.status_code != 200:
            DEBUG(r.json())
            if r.json()['error_description'] == 'code is invalid':
                print ("An error occurred")
                quit(1)
        else:
            DEBUG(r.content)
        return


    def get_tax_summary(self, accessToken, utr, tax_year):
        '''
        @oaram accessToken string
        @param  utr string e.g. "2234567890"
        @param  tax_year string e.g. "2016-17"
        '''
        request = "ttps://test-api.service.hmrc.gov.uk/individual-tax/sa/{}/annual-summary/{}".format(utr,tax_year)
        DEBUG(request)
        r = requests.get(request, \
            headers = {"Accept":"application/vnd.hmrc.1.1+json", \
            "Authorization": "Bearer " + accessToken})
        DEBUG(r.request.headers)
        DEBUG(r.content)
        DEBUG(r.json())
        return r.json()


    def refresh_token(self,refresh_token ):
        '''
        To refresh the access_token, submit the expired token's corresponding refresh_token to our token endpoint using grant_type of refresh_token.

        You can only use a refresh_token once. When you refresh an access_token, it invalidates the original access_token immediately if it has not already expired.
        Example response
        {
          "access_token": "unJkSs5cvs8CS9E4DLvTkNhcRBq9BwUPm23cr3pF",
          "token_type": "bearer",
          "expires_in": 14400,
          "refresh_token": "jPtmQuLtKmLhGURk8CmR2sWPmffBhDhPyFEEF4ay"
        }
        TODO: This
        @param string refresh_token
        '''
        r = requests.post("https://test-api.service.hmrc.gov.uk/oauth/token", \
            data = {"client_id":app_creds.client_id,"client_secret": app_creds.client_secret, \
            "code":code,"grant_type": "refresh_token", "refresh_token":refresh_token \
            })
        if r.status_code != 200:
            DEBUG(r.json())
            if r.json()['error_description'] == 'code is invalid':
                print ("An error occurred")
                quit(1)
        else:
            DEBUG(r.content)
        return


    def start_server(self):
        '''
        This just processes one request and returns the access token
        '''
        port = 9090
        try: server.socket.close()
        except: pass
        server = HTTPServer(('', port), RequestHandler)
        server.handle_request()
        server.socket.close()
        return server.access_token


    def get_gnucash_data(self):
        '''
        Get the required data from gnucash. For unknown value of "required data"
        '''
        pass



    def get_annual_summary(self, accessToken, nino, selfEmploymentId, dates):
        '''
        @param String access token
        @param Sing National Insurance Number
        @param String selfEmploymentId
        @param String dates ?
        '''

        r = requests.get( "https://test-api.service.hmrc.gov.uk/self-assessment/ni/{}/self-employments/{}/{}"\
            .format(nino,selfEmploymentId,dates),\
        headers = {"Accept":"application/vnd.hmrc.2.0+json", \
            "Authorization": "Bearer " + accessToken})
        if r.status_code == 404:
            print (r.status_code)
            return None
        if r.ok:
            return r.json()
        else:
            DEBUG(r.status_code)
            DEBUG(r.content)
            return None

    def list_update_periods(self, accessToken, nino, selfEmploymentId):
        '''
        @param String access token
        @param String National Insurance Number
        @param String selfEmploymentId from a previous query
        '''
        r = requests.get( "https://test-api.service.hmrc.gov.uk//self-assessment/ni/{}/self-employments/{}/periods"\
            .format(nino, selfEmploymentId),
            headers = {"Accept":"application/vnd.hmrc.2.0+json", \
            "Authorization": "Bearer " + accessToken})
        if r.ok:
            return r.json()
        else:
            DEBUG(r.status_code)
            DEBUG(r.content)
            return None


    def obligations_to_ical(self, obligations, file_path):
        '''
        Requires: pip install icalendar
        Create or update an ical file that the user can add to their calendar.
        Requires icalendar, pip install  icalendar --user
        Should possible default to putting the calendar in the same place as
        the GnuCash account.
        Reminders could be from end date up to due date, with perhaps increasing
        priority or frequency.

        Sample Calendar:
        BEGIN:VCALENDAR
        PRODID:-//K Desktop Environment//NONSGML KOrganizer 5.7.3//EN
        VERSION:2.0
        X-KDE-ICAL-IMPLEMENTATION-VERSION:1.0
        BEGIN:VEVENT
        DTSTAMP:20190416T105522Z
        CREATED:20190416T105504Z
        UID:2de7dbb0-7b92-408c-96da-d3706ab6cca5
        LAST-MODIFIED:20190416T105522Z
        SUMMARY:TEst
        DTSTART;VALUE=DATE:20171124
        DTEND;VALUE=DATE:20171125
        TRANSP:OPAQUE
        BEGIN:VALARM
        DESCRIPTION:
        ACTION:DISPLAY
        TRIGGER;VALUE=DURATION:-P1D
        X-KDE-KCALCORE-ENABLED:TRUE
        END:VALARM
        END:VEVENT
        END:VCALENDAR

        @param Json Obligations obtained from get_business_obligations()
        @param String Path to save to

        '''
        from icalendar import Calendar, Alarm, Event,vCalAddress, vText, vDatetime
        import pytz
        import uuid

        cal = Calendar()
        cal.add('prodid', '-//My calendar product//mxm.dk//')
        cal.add('version', '2.0')
        for obligation in obligations['obligations']:
            event = Event()
            if obligation['met'] == False:
                event['uid'] = str(uuid.uuid1())
                event['DTSTAMP'] = datetime.now()
                event.add('summary', "Tax submission period")
                event.add('dtstart', (datetime.strptime(obligation['end'],"%Y-%m-%d")))
                event.add('dtstamp', datetime.now())
                event.add('dtend',(datetime.strptime(obligation['due'],"%Y-%m-%d")))
                event.add('description', "Submit your quarterly tax return")
                alarm = Alarm()
                alarm.add('ACTION', 'DISPLAY')
                alarm.add('TRIGGER', datetime.strptime(obligation['due'],"%Y-%m-%d"))
                alarm.add('DESCRIPTION', 'Last day to file!')
                DEBUG(obligation['due'])
                event.add_component(alarm)
                cal.add_component(event)
                last = Event()
                last['uid'] = str(uuid.uuid1())
                last['DTSTAMP'] = datetime.now()
                last.add('summary', "LAST DAY TO FILE!")
                last.add('dtstart', (datetime.strptime(obligation['due'],"%Y-%m-%d")))
                last.add('dtend', (datetime.strptime(obligation['due'],"%Y-%m-%d")))
                cal.add_component(last)
        f = open('tax_obligations.ics', 'wb')
        f.write(cal.to_ical())
        f.close()
        return


    def get_business_obligations(self, access_token, nino, selfEmploymentId):
        '''
        Obtain the tax obligations for a business. The returned json looks like:

        {'obligations': [{'start': '2017-04-06', 'end': '2017-07-05', 'due': '2017-08-05', 'met': True}, {'start': '2017-07-06', 'end': '2017-10-05', 'due': '2017-11-05', 'met': False}, {'start': '2017-10-06', 'end': '2018-01-05', 'due': '2018-02-05', 'met': False}, {'start': '2018-01-06', 'end': '2018-04-05', 'due': '2018-05-05', 'met': False}]}
        It can be seen that the first quarter has been paid, the next submission was  due on 2017-11-05
        These data can be fed into semi permenant storage or a diary to remind the user when to pay perhaps.
        @param String Access token obtained from get_auth_code()
        @param String Naional Insurance Number of the person
        @param String The self emplymentd ID related to the business.

        '''
        r = requests.get( f"https://test-api.service.hmrc.gov.uk//self-assessment/ni/{nino}/self-employments/{selfEmploymentId}/periods", \
            headers = {"Accept":"application/vnd.hmrc.2.0+json", \
            "Authorization": "Bearer " + access_token})
        if r.ok:
            return r.json()
        else:
            DEBUG(r.status_code)
            DEBUG(r.content)
            return None


    def get_vat_obligations(self, accessToken, vrn,):
        DEBUG(accessToken)
        DEBUG(vrn)
        r = requests.get( "https://test-api.service.hmrc.gov.uk/organisations/vat/{}/obligations".format(vrn),headers = {"Accept":"application/vnd.hmrc.1.0+json", \
            "Authorization": "Bearer {}".format(accessToken)},
            params = {"status":"O"})
        DEBUG(r.status_code)
        DEBUG(r.content)
        if r.ok:
            return r.json()
        else: return None

################## END pyselftax ###############################################


if __name__ == "__main__":
    DEBUG("{} {}".format(test_user['userId'], test_user['password']))
    app = Pyselftax()

    # Do this once to get a test user
    if False: # Set True to run
        token = app.get_access_token()
        #At this point we have an auth token, so we can now query the API
        # Just run the create user methods once and paste in up top or cat to a file etc.
        test_user = app.create_test_user(token)
        #vat_test_user = app.create_test_vat_individual() # Works


    code_url = app.get_auth_code()
    code = app.start_server()
    DEBUG(code)
    token = app.get_access_token(code)
    DEBUG(token)
    # The below are commented so as to run just the one query at a time. Some data are hard coded! :)
    app.add_business(token, test_user['nino']) # Works
    #print(app.get_tax_summary(token, test_user['saUtr'], "2017-18")) # Nope
    #print(app.get_individual_income(token, test_user['nino'],'XDIS41069900810' , "2017-18")) #

    # Json data representing a short type income statement
    json_data = {
        "from": "2017-04-06",
        "to": "2017-07-04",
        "incomes": {
        "turnover": {
          "amount": 100.25
        },
        "other": {
          "amount": 100.25
        }
        },
        "consolidatedExpenses": 100.25
        }

    # So this all needs to joined up to make a useful application.
    #app.income_for_period_add(token, test_user['nino'],'XDIS41069900810', json_data) #
    #print(app.list_all_businesses(token, test_user['nino'])) #
    #print(app.list_update_periods(token, test_user['nino'],test_user['XGIT00396400433'])) #
    #print (app.get_business_obligations(token, test_user['nino'], test_user['mtdItId']))
    #print(app.get_income_for_period(token, test_user['nino'], 'XDIS41069900810','2017-04-06_2017-07-04')) #
    #print(app.get_annual_summary(token, test_user['nino'], 'XDIS41069900810', "2017-18")) #
    #print(app.get_vat_obligations(token, vat_user.test_vat_user['vrn'])) # Nope CLIENT_OR_AGENT_NOT_AUTHORISED
    obs = {'obligations': [{'start': '2017-04-06', 'end': '2017-07-05', 'due': '2017-08-05', 'met': True}, {'start': '2017-07-06' , 'end': '2017-10-05', 'due': '2017-11-05', 'met': False}, {'start': '2017-10-06', 'end': '2018-01-05', 'due': '2018-02-05', 'met': False}, {'start': '2018-01-06', 'end': '2018-04-05', 'due': '2018-05-05', 'met': False}]}

    #app.obligations_to_ical(obs, "tax_due.ical") # Nope
