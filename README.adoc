= HMRC Making Tax Diffcult (MTD)

This project is intended to get data from GnuCash and push that to HMRC's MTD API.

This project only deals with Self Employment data that are meant to pre-populate the user's tax return. The user still has to check and finalise this tax return each year in the same way as is done currently. So what's the point of this then? None, but that's what HMRC want to do so we all have to comply. Baaah.

It also means that we all need to buy new software to do this and those of us using open source or use Linux will either have to retire, get a real job or become programmers and write our own API interface. I chose the latter since I already am a programmer (sortof).

It imports app_creds(.py), which isn't on GitLab and needs to be created when registering the application.
This is currently just a bunch of thrown-together test code and doesn't do anything useful. I (we) need to tie it all together to query the parts so-as to congeal the data into something useful.  Some of the methods don't work as some of the HMRC docs appear to be old|wrong|complete bollocks.

It assumes you have an account tree based on:

https://www.gnucash.org/docs/v3/C/gnucash-guide/appendixc_vat1.html

It doesn't deal with VAT, yet, just earnings & expenses. Although I guess VAT submissions would not be difficult to add. It only deals with UK transactions in GBP currently.  Patches welcome.

I envision that a module will be written (in C++ if you are offering) that is an integral part of GnuCash to replace this bridging software.

It's unlikely this will be it's final application name as I haven't discussed with the GnuCash team yet so this is just a place-holder while I explore the API.

I can add team members for those that want to access the API and develop this further. Send me your e-mail along with some justifiable reason to join the team.  Or better yet, just clone the repo. and leave me alone. ;)


== Gnucash Interface

gnucash_interface.py works with python2 for version for GnuCash using python2 bindings, not with python3 unless|until PR 484 is pulled into maint.
This may no longer be true though as I've been idle (on this project) for the last three years.

A sample quarter summary:
----
"from","01/01/18"
"to","31/03/18"
"Gross Sales","$1,200.00"
"Net Sales","$1,000.00"
"GST on Sales","$200.00"
"Gross Purchases","$600.00"
"Net Purchases","$500.00
"GST on Purchases","$100.00"
----

The app needs to get, from HMRC, their obligations for the current year.  This will show what the periods are, which have been paid, which are due, and when.  The income for each of the periods can then be obtained from GnuCash and after the user checks and authorises, the P+L is posted to HMRC.  Since HMRC holds the obligations there's probably no need to store this locally.

The HMRC credentials will need to be stored somewhere.  I stalled at this point.
