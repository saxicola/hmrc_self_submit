# Command line to compile is:
#gcc -o gnucash_hmrc  gnucash_hmrc.cpp -Wall `pkg-config --cflags --libs gtk+-3.0` -export-dynamic -rdynamic

cmake_minimum_required (VERSION 2.8)
project (HMRC-MTD)


# Use the package PkgConfig to detect GTK+ headers/library files
FIND_PACKAGE(PkgConfig REQUIRED)
PKG_CHECK_MODULES(GTK REQUIRED gtk+-3.0)


SET(CMAKE_SHARED_LIBRARY_LINK_C_FLAGS)
SET(CMAKE_INCLUDE_CURRENT_DIR ON)
SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c11 -export-dynamic -rdynamic")
ADD_DEFINITIONS( -DHAVE_GUILE18 )

# Setup CMake to use GTK+, tell the compiler where to look for headers
# and to the linker where to look for libraries
INCLUDE_DIRECTORIES(
    /usr/local/include/gnucash
    /home/mikee/Projects/gnucash/libgnucash/core-utils
    /home/mikee/Projects/gnucash/libgnucash/engine
    ${GTK_INCLUDE_DIRS}
    include
    )

LINK_DIRECTORIES(/usr/local/lib64/gnucash/
    ${GTK_LIBRARY_DIRS}
    /usr/lib64
    lib
    /usr/lib64/gnucash
    )



# Add other flags to the compiler
ADD_DEFINITIONS(${GTK_CFLAGS_OTHER})

ADD_EXECUTABLE(gnucash_hmrc  gnucash_hmrc.c)


# Link the target to the GTK+ libraries
TARGET_LINK_LIBRARIES(gnucash_hmrc
        ${GTK_LIBRARIES}
        requests
        curl
        /usr/lib64/gnucash
        )
