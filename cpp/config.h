/* Template file for processing by the cmake command CONFIGURE_FILE */

/* If we're using this file it's a CMAKE build, so say so */
#define CMAKE_BUILD 1

/* Define if building universal (internal helper macro) */
/* #undef AC_APPLE_UNIVERSAL_BUILD */

/* Include pthread support for binary relocation? */
#define BR_PTHREAD 1

/* Use binary relocation? */
#define ENABLE_BINRELOC

/* always defined to indicate that i18n is enabled */
#define ENABLE_NLS 1

/* Don't use deprecated gdk functions */
/* #undef GDK_DISABLE_DEPRECATED */

/* Don't use deprecated gdk-pixbuf functions */
/* #undef GDK_PIXBUF_DISABLE_DEPRECATED */

/* Using GDK Quartz (not X11) */
/* #undef GDK_QUARTZ */

/* Name of our gettext-domain */
#define GETTEXT_PACKAGE "gnucash"

/* Cocoa/Nexstep/GnuStep framework */
/* #undef GNC_PLATFORM_COCOA */

/* Darwin-based OS */
/* #undef GNC_PLATFORM_DARWIN */

/* Running on OSX, either X11 or Quartz */
/* #undef GNC_PLATFORM_OSX */
/* #undef PLATFORM_OSX */

/* POSIX-compliant OS */
#define GNC_PLATFORM_POSIX 1

/* Microsoft Windows OS */
/* #undef GNC_PLATFORM_WINDOWS */

/* using GNOME */
/* #undef GNOME */

/* Don't use deprecated gnome functions */
/* #undef GNOME_DISABLE_DEPRECATED */

/* GnuCash build identification, which defaults to a description of the vcs
   commit from which gnucash is built. Distributions may want to insert a
   package management based version string instead. */
#define GNUCASH_BUILD_ID ""

/* Most recent stable GnuCash series */
#define GNUCASH_LATEST_STABLE_SERIES "3.x"

/* GnuCash Major version number */
#define GNUCASH_MAJOR_VERSION 3

/* GnuCash Micro version number */
#define GNUCASH_MICRO_VERSION 

/* GnuCash Minor version number */
#define GNUCASH_MINOR_VERSION 5

/* GnuCash Nano version number */
#define GNUCASH_NANO_VERSION 

/* GnuCash earliest compatible databaseversion number */
#define GNUCASH_RESAVE_VERSION 19920

/* Don't use deprecated gtk functions */
/* #undef GTK_DISABLE_DEPRECATED */

/* Don't use deprecated glib functions */
/* #undef G_DISABLE_DEPRECATED */

/* Define to 1 if you have the `bind_textdomain_codeset' function. */
#define HAVE_BIND_TEXTDOMAIN_CODESET 1

/* define if the Boost library is available */
#define HAVE_BOOST

/* Define to 1 if you have the `chown' function. */
#define HAVE_CHOWN 1

/* define if the compiler supports basic C++11 syntax */
/* #undef HAVE_CXX11 */

/* Define to 1 if you have the <dbi/dbi.h> header file. */
#define HAVE_DBI_DBI_H 1

/* Define to 1 if you have the `dcgettext' function. */
#define HAVE_DCGETTEXT 1

/* Define to 1 if you have the <dirent.h> header file. */
#define HAVE_DIRENT_H 1

/* Define to 1 if you have the `dlerror' function. */
#define HAVE_DLERROR 1

/* Define to 1 if you have the <dlfcn.h> header file. */
#define HAVE_DLFCN_H 1

/* Define to 1 if you have the `dlsym' function. */
#define HAVE_DLSYM 1

/* Define to 1 if you have the <dl.h> header file. */
/* #undef HAVE_DL_H */

/* Define to 1 if you have the `gethostid' function. */
#define HAVE_GETHOSTID 1

/* Define to 1 if you have the `gethostname' function. */
#define HAVE_GETHOSTNAME 1

/* Define to 1 if you have the `getppid' function. */
#define HAVE_GETPPID 1

/* Define if the GNU gettext() function is already present or preinstalled. */
#define HAVE_GETTEXT 1

/* Define to 1 if you have the `gettimeofday' function. */
#define HAVE_GETTIMEOFDAY 1

/* Define to 1 if you have the `getuid' function. */
#define HAVE_GETUID 1

/* Configure g_settings_list_keys deprecation */
#define HAVE_GLIB_2_46 1

/* Define to 1 if you have the <glob.h> header file. */
#define HAVE_GLOB_H 1

/* Define to 1 if you have the `gmtime_r' function. */
#define HAVE_GMTIME_R 1

/* System has gnome-keyring 0.6 or better */
/* #undef HAVE_GNOME_KEYRING */

/* System has HtmlHelpW */
/* #undef HAVE_HTMLHELPW */

/* Define to 1 if you have the <inttypes.h> header file. */
#define HAVE_INTTYPES_H 1

/* Define if you have <langinfo.h> and nl_langinfo(D_FMT). */
#define HAVE_LANGINFO_D_FMT 1

/* Define if your <locale.h> file defines LC_MESSAGES. */
#define HAVE_LC_MESSAGES 1

/* Define to 1 if you have the `pthread' library (-lpthread). */
#define HAVE_LIBPTHREAD 1

/* System has libsecret 0.18 or better */
#define HAVE_LIBSECRET 1

/* Define to 1 if you have the <limits.h> header file. */
#define HAVE_LIMITS_H 1

/* Define to 1 if you have the `link' function. */
#define HAVE_LINK 1

/* Define to 1 if you have the <locale.h> header file. */
#define HAVE_LOCALE_H 1

/* Define to 1 if you have the <ltdl.h> header file. */
#define HAVE_LTDL_H 1

/* Define to 1 if you have the <mcheck.h> header file. */
/* #undef HAVE_MCHECK_H */

/* Define to 1 if you have the `memcpy' function. */
#define HAVE_MEMCPY 1

/* Define to 1 if you have the <memory.h> header file. */
#define HAVE_MEMORY_H 1

/* System has an OS X Key chain */
/* #undef HAVE_OSX_KEYCHAIN */

/* Define to 1 if you have the <pow.h> header file. */
/* #undef HAVE_POW_H */

/* Define to 1 if you have the `pthread_mutex_init' function. */
#define HAVE_PTHREAD_MUTEX_INIT 1

/* Have PTHREAD_PRIO_INHERIT. */
#define HAVE_PTHREAD_PRIO_INHERIT 1

/* If available, contains the Python version number currently in use. */
/* #undef HAVE_PYTHON */

/* Define if scanf supports %I64d conversions. */
/* #undef HAVE_SCANF_I64D */

/* Define if scanf supports %lld conversions. */
#define HAVE_SCANF_LLD 1

/* Define if scanf supports %qd conversions. */
/* #undef HAVE_SCANF_QD */

/* Define to 1 if you have the `setenv' function. */
#define HAVE_SETENV 1

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define to 1 if you have the `stpcpy' function. */
#define HAVE_STPCPY 1

/* Define to 1 if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define to 1 if you have the `strptime' function. */
#define HAVE_STRPTIME 1

/* Define if you have the tm_gmtoff member of struct tm. */
#define HAVE_STRUCT_TM_GMTOFF 1

/* Define to 1 if you have the <sys/stat.h> header file. */
#define HAVE_SYS_STAT_H 1

/* Define to 1 if you have the <sys/times.h> header file. */
#define HAVE_SYS_TIMES_H 1

/* Define to 1 if you have the <sys/time.h> header file. */
#define HAVE_SYS_TIME_H 1

/* Define to 1 if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H 1

/* Define to 1 if you have the <sys/wait.h> header file. */
#define HAVE_SYS_WAIT_H 1

/* Define to 1 if you have the `timegm' function. */
#define HAVE_TIMEGM 1

/* Define to 1 if you have the `towupper' function. */
#define HAVE_TOWUPPER 1

/* Define to 1 if you have the <unistd.h> header file. */
#define HAVE_UNISTD_H 1

/* Define to 1 if you have the <utmp.h> header file. */
#define HAVE_UTMP_H 1

/* Define to 1 if you have the <wctype.h> header file. */
#define HAVE_WCTYPE_H 1

/* Define to 1 if you have the file `/usr/include/gmock/gmock.h'. */
/* #undef HAVE__USR_INCLUDE_GMOCK_GMOCK_H */

/* Define to 1 if you have the file `/usr/include/gtest/gtest.h'. */
/* #undef HAVE__USR_INCLUDE_GTEST_GTEST_H */

/* Define to 1 if you have the file `/usr/src/gmock/gmock-all.cc'. */
/* #undef HAVE__USR_SRC_GMOCK_GMOCK_ALL_CC */

/* Define to 1 if you have the file `/usr/src/gmock/src/gmock-all.cc'. */
/* #undef HAVE__USR_SRC_GMOCK_SRC_GMOCK_ALL_CC */

/* Define to 1 if you have the file `/usr/src/gtest/gtest-main.cc'. */
/* #undef HAVE__USR_SRC_GTEST_GTEST_MAIN_CC */

/* Define to 1 if you have the file `/usr/src/gtest/src/gtest-all.cc'. */
/* #undef HAVE__USR_SRC_GTEST_SRC_GTEST_ALL_CC */

/* Define to the sub-directory where libtool stores uninstalled libraries. */
#define LT_OBJDIR ".libs/"

/* Name of package */
#define PACKAGE "gnucash"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "https://bugs.gnucash.org"

/* Define to the full name of this package. */
#define PACKAGE_NAME "GnuCash"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "GnuCash 3.5"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "gnucash"

/* Define to the home page for this package. */
#define PACKAGE_URL "https://www.gnucash.org/"

/* Define to the version of this package. */
#define PACKAGE_VERSION "3.5"

/* Define to necessary symbol if this constant uses a non-standard name on
   your system. */
/* #undef PTHREAD_CREATE_JOINABLE */

/* Result of LibOFX Bug 39 detection (see
  https://sourceforge.net/p/libofx/bugs/39/ for details). */
#define HAVE_OFX_BUG_39 1

/* Name of package containing qt3-wizard. */
#define QT3_WIZARD_PACKAGE "aqbanking"

/* We are not using Register2 */
/* #undef REGISTER2_ENABLED */

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1

/* We are using EFence */
/* #undef USE_EFENCE */

/* Enable extensions on AIX 3, Interix.  */
#ifndef _ALL_SOURCE
#define _ALL_SOURCE 1
#endif
/* Enable GNU extensions on systems that have them.  */
#ifndef _GNU_SOURCE
#define _GNU_SOURCE 1
#endif
/* Enable threading extensions on Solaris.  */
#ifndef _POSIX_PTHREAD_SEMANTICS
#define _POSIX_PTHREAD_SEMANTICS 1
#endif
/* Enable extensions on HP NonStop.  */
#ifndef _TANDEM_SOURCE
#define _TANDEM_SOURCE 1
#endif
/* Enable general extensions on Solaris.  */
#ifndef __EXTENSIONS__
#define __EXTENSIONS__ 1
#endif


/* Version number of package */
#define VERSION "3.5"

/* Define WORDS_BIGENDIAN to 1 if your processor stores words with the most
   significant byte first (like Motorola and SPARC, unlike Intel). */
#if defined AC_APPLE_UNIVERSAL_BUILD
# if defined __BIG_ENDIAN__
#  define WORDS_BIGENDIAN 1
# endif
#else
# ifndef WORDS_BIGENDIAN
/* #undef WORDS_BIGENDIAN */
# endif
#endif

/* Define to 1 if on MINIX. */
/* #undef _MINIX */

/* Define to 2 if the system does not provide POSIX.1 features except with
   this defined. */
/* #undef _POSIX_1_SOURCE */

/* Define to 1 if you need to in order for `stat' and other things to work. */
/* #undef _POSIX_SOURCE */

/* Definitions for what WEBKIT version we're compiling against: */
#define WEBKIT2_4 1
/* #undef WEBKIT2_3 */
/* #undef WEBKIT1 */

/* Definitions for all OS */
/* From cutecash */
//#define HAVE_LIBQOF /**/
//#define QOF_DISABLE_DEPRECATED 1
//#define GNC_NO_LOADABLE_MODULES 1
