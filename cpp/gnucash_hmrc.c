


#include <stdarg.h>
#include <stdio.h>
#include <gtk/gtk.h>

#include <config.h>

#include <glib/gi18n.h>
#include <libguile.h>
#include "requests.h"

//#include "qofutil.h"
#include "qof.h"

#include "Query.h"
#include "qof.h"
//#include "gnc-component-manager.h"
//#include "gnc-ui.h"
//#include "gnc-gui-query.h"
#include "gnc-prefs.h"
//#include "gnc-ui-util.h"
#include "gnc-date.h"
#include "gnc-date-edit.h"
//#include "gnc-amount-edit.h"
#include "gnc-session.h"
#include "gncOwner.h"

//#include "gnc-plugin-page.h"
//#include "gnc-general-search.h"

#include "qofbook.h"
#include "Account.h"


GtkWidget *g_lbl_hello;
GtkWidget *g_lbl_count;

// For the GtkTreeView
enum
{
    FILE_ITEM = 0,
    FULL_PATH,
    NUM_COLUMNS
};

struct config{
char * account_file;
// etc...
};

// Signal Handlers

void window_delete()
{

}

// called when window is closed
void on_destroy()
{
    gtk_main_quit();
}


void on_combo_last_files_changed( GtkComboBox *widget,gpointer   data )
{
    gint  item_num = 0;     // selected item number from text combo box
    gchar *item_text = 0;   // selected item text from text combo box
    GtkTreeModel   *model;
    GtkTreeIter   iter;
    // get selected item number from GtkComboBoxText object
    item_num = gtk_combo_box_get_active(GTK_COMBO_BOX(widget));
    // print the selected number to the dynamic string
    g_print("%d\n", item_num);
    // Get the list store.
    if( gtk_combo_box_get_active_iter( widget, &iter ) )
    {
        /* Obtain data model from combo box. */
        model = gtk_combo_box_get_model( widget );

        /* Obtain string from model. */
        gtk_tree_model_get( model, &iter, FULL_PATH, &item_text, -1 );
    }
    g_print("%s\n", item_text);
}

void on_submit_earnings()
{}

void on_submit_vat()
{}

void on_button_query_clicked()
{
    /* Test Code */
    req_t req;
    Account *account;// = Something something ??????
    //QofBook *book = gnc_account_get_book(account);

    return;
    CURL *curl = requests_init(&req); /* setup */
    requests_get(curl, &req, "http://example.com"); /* submit GET request */
    printf("Request URL: %s\n", req.url);
    printf("Response Code: %lu\n", req.code);
    printf("Response Body:\n%s", req.text);
    requests_close(&req); /* clean up */
    return;
}

void on_button_close(__attribute__((unused)) GtkWidget *widget,
                        __attribute__((unused)) gpointer   data)
{
    g_print("Quit Clicked\n");
    gtk_main_quit();
}

// MAIN, YAYYYY!
int main (int argc, char ** argv)
{
    GtkBuilder      *builder;
    GtkWidget       *window;
    GtkListStore    *recent_files_list;
    GtkComboBox     *recent_files_dropdown;
    GtkTreeIter     iter;


    gtk_init(&argc, &argv);

    builder = gtk_builder_new();
    gtk_builder_add_from_file (builder, "../python/ui/main_window.glade", NULL);
    window = GTK_WIDGET(gtk_builder_get_object(builder, "window1"));
    gtk_builder_connect_signals(builder, NULL);

    recent_files_list = GTK_LIST_STORE(gtk_builder_get_object(builder, "liststore_last_files"));
    recent_files_dropdown = GTK_COMBO_BOX(gtk_builder_get_object(builder,"combo_last_files"));
    gtk_list_store_append(recent_files_list, &iter);
    gtk_list_store_set(recent_files_list, &iter, 1, "FULSOME", -1); // Test
    gtk_list_store_append(recent_files_list, &iter);
    gtk_list_store_set(recent_files_list, &iter, 1, "FUBAR", -1); // Test

    g_object_unref(builder);

    gtk_widget_show_all(window);
    gtk_main();

    return 0;
}
// END MAIN SAD-FACE
